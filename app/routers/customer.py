from app import app
from app.controllers import customer
from flask import Blueprint, request

customer_blueprint = Blueprint("customer",__name__)

@app.route("/users",methods=["GET"])
def show_Users():
    return customer.showUsers()

@app.route("/user",methods=["GET"])
def show_User():
    params = request.json
    return customer.showUsersById(**params)

@app.route("/user/insert",methods=["POST"])
def insert_User():
    params = request.json
    return customer.insertUser(**params)

@app.route("/user/update",methods=["POST"])
def update_User():
    params = request.json
    return customer.updateUserById(**params)

@app.route("/user/delete",methods=["POST"])
def delete_User():
    params = request.json
    return customer.deleteUserById(**params)

@app.route("/user/requesttoken",methods=["GET"])
def requestToken():
    params = request.json
    return customer.token(**params)