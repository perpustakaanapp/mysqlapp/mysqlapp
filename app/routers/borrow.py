from app import app
from app.controllers import borrow
from flask import Blueprint, request

borrow_blueprint = Blueprint("borrow",__name__)

@app.route("/borrow",methods=["GET"])
def show_Borrows():
    params = request.json
    return borrow.showBorrowByEmail(**params)

@app.route("/borrow/insert",methods=["POST"])
def insert_Borrow():
    params = request.json
    return borrow.insertBorrow(**params)

@app.route("/borrow/update",methods=["POST"])
def update_Borrow():
    params = request.json
    return borrow.updateBorrow(**params)

