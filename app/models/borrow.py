from sqlalchemy import Column, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Borrow (Base):
    __tablename__ = 'borrows'
    borrowid = Column(Integer, primary_key =  True)
    userid = Column(Integer)
    bookid = Column(Integer)
    borrowdate = Column(Date)
    isactive = Column(Integer)

    def __init__(self):
        try:
            self.engine = create_engine('mysql+mysqlconnector://umfcevx1dbmckgzp:YmSYCTdzNBnrs27tXT3J@bhiuyl3cwuwthqt9qetv-mysql.services.clever-cloud.com/bhiuyl3cwuwthqt9qetv', echo = True)
        except Exception as e:
            print(e)
        else:
            self.Session = sessionmaker(bind=self.engine)
            self.session = self.Session()
    
    def closeConection(self):
        self.session.close()