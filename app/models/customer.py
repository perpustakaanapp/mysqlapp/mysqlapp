from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Customer(Base):
    __tablename__ = 'customers'
    userid = Column(Integer, primary_key =  True)
    username = Column(String)
    namadepan = Column(String)
    namabelakang = Column(String)
    email = Column(String)

    def __init__(self):
        try:
            self.engine = create_engine('mysql+mysqlconnector://umfcevx1dbmckgzp:YmSYCTdzNBnrs27tXT3J@bhiuyl3cwuwthqt9qetv-mysql.services.clever-cloud.com/bhiuyl3cwuwthqt9qetv', echo = True)
        except Exception as e:
            print(e)
        else:
            self.Session = sessionmaker(bind=self.engine)
            self.session = self.Session()
    
    def closeConection(self):
        self.session.close()