from app.models.customer import Customer
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mydb = Customer()

def showUsers():
        try:
            dbresult =  mydb.session.query(Customer).all()
            result = []
            for row in dbresult:
                user = {
                    "id" : row.userid,
                    "username" : row.username, 
                    "namadepan" : row.namadepan, 
                    "namabelakang" : row.namabelakang,
                    "email" : row.email
                }
                result.append(user)
        
            
        except Exception as e:
            return jsonify({"message":e})
        else:
            return jsonify(result)
        finally:
            mydb.closeConection()        

def showUsersById(**params):
        try:
            dbresult = mydb.session.query(Customer).filter(Customer.userid==params["userid"])

            result = []
            for row in dbresult:
                user = {
                    "id" : row.userid,
                    "username" : row.username, 
                    "namadepan" : row.namadepan, 
                    "namabelakang" : row.namabelakang,
                    "email" : row.email
                }
                result.append(user)
            
            
        except Exception as e:
            return jsonify({"message":e})
        else:
            return jsonify(result)
        finally:
            mydb.closeConection()

def insertUser(**params):
        try:
            mydb.session.add(Customer(**params))
            mydb.session.commit()
        except Exception as e:
            return jsonify({"message":e})
        else:
            return jsonify({"message":"Berhasil"})
        finally:
            mydb.closeConection()


def updateUserById(**params):
        try:
            result =  mydb.session.query(Customer).filter(Customer.userid==params["userid"]).one()
            result.username = params["namadepan"]
            result.namadepan = params["namadepan"]
            result.namabelakang = params["namabelakang"]
            result.email = params["email"]
            mydb.session.commit()
        except Exception as e:
            return jsonify({"message":e})
        else:
            return jsonify({"message":"Berhasil"})
        finally:
            mydb.closeConection()

def deleteUserById(**params):
        try:
            result =  mydb.session.query(Customer).filter(Customer.userid==params["userid"]).one()
            mydb.session.delete(result)
            mydb.session.commit()
        except Exception as e:
            return jsonify({"message":e})
        else:
            return jsonify({"message":"Berhasil"})
        finally:
            mydb.closeConection()

def token(**params):
        try:
            dbresult = mydb.session.query(Customer).filter(Customer.email==params["email"])

            if dbresult is not None:
                    
                for row in dbresult:
                    user = {
                        "username" : row.username, 
                        "email" : row.email
                    }

                expires = datetime.timedelta(days=1)
                access_token = create_access_token(user,fresh=True,expires_delta=expires)

                data = {
                    "data":user,
                    "token_access":access_token
                }
            else:
                data = {
                    "message":"Email tidak ditemukan"                }
        except Exception as e:
            return jsonify({"message":e})
        else:
            return jsonify(data)
        finally:
            mydb.closeConection()