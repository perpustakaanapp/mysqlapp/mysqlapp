from app.models.borrow import Borrow
from app.models.customer import Customer
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime, requests

mydb = Borrow()
customerdb = Customer()

@jwt_required()
def showBorrowByEmail(**params):
        try:
            dbresult = mydb.session.query(Borrow).join(Customer).filter(Borrow.email==params["email"])

            result = []
            for row in dbresult:
                for brw in row.borrows:
                        
                    pinjaman = {
                        "borrowid" : brw.userid,
                        "userid" : brw.userid, 
                        "bookid" : brw.bookid, 
                        "borrowdate" : brw.borrowdate,
                        "isactive" : brw.isactive
                    }
                    result.append(pinjaman)
            
            
        except Exception as e:
            return jsonify({"message":e})
        else:
            return jsonify(result)
        finally:
            mydb.closeConection()

@jwt_required()
def insertBorrow(**params):
        try:
            mydb.session.add(Borrow(**params))
            mydb.session.commit()
        except Exception as e:
            return jsonify({"message":e})
        else:
            return jsonify({"message":"Berhasil"})
        finally:
            mydb.closeConection()

@jwt_required()
def updateBorrowById(**params):
        try:
            result =  mydb.session.query(Borrow).filter(Borrow.borrowid==params["borrowid"]).one()
            result.isactive = params["isactive"]
            mydb.session.commit()
        except Exception as e:
            return jsonify({"message":e})
        else:
            return jsonify({"message":"Berhasil"})
        finally:
            mydb.closeConection()
